-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 01, 2019 at 09:37 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aslab`
--
CREATE DATABASE IF NOT EXISTS `aslab` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `aslab`;

-- --------------------------------------------------------

--
-- Table structure for table `ahp_aslab_terpilih`
--

CREATE TABLE `ahp_aslab_terpilih` (
  `id` int(11) NOT NULL,
  `calon` int(11) NOT NULL,
  `nilai` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_aslab_terpilih`
--

INSERT INTO `ahp_aslab_terpilih` (`id`, `calon`, `nilai`) VALUES
(1, 29, 0.043983),
(2, 30, 0.0458),
(3, 31, 0.047963),
(4, 32, 0.045133),
(5, 33, 0.047646),
(6, 34, 0.043965),
(7, 35, 0.046108),
(8, 36, 0.044883),
(9, 37, 0.04747),
(10, 38, 0.044478),
(11, 39, 0.046578),
(12, 40, 0.046326),
(13, 41, 0.048586),
(14, 42, 0.040375),
(15, 43, 0.044663),
(16, 44, 0.044677),
(17, 45, 0.043669),
(18, 46, 0.043026),
(19, 47, 0.047519),
(20, 48, 0.041264),
(21, 49, 0.046579),
(22, 50, 0.049309);

-- --------------------------------------------------------

--
-- Table structure for table `ahp_nilai_tes`
--

CREATE TABLE `ahp_nilai_tes` (
  `id` int(11) NOT NULL,
  `calon` int(11) NOT NULL,
  `tulis` double DEFAULT NULL,
  `wawancara` double DEFAULT NULL,
  `akademik` double DEFAULT NULL,
  `micro` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_nilai_tes`
--

INSERT INTO `ahp_nilai_tes` (`id`, `calon`, `tulis`, `wawancara`, `akademik`, `micro`) VALUES
(236, 29, 30, 3.15, 3.5, 70.1),
(237, 30, 36, 2, 3.63, 64.15),
(238, 31, 38, 3.5, 3.5, 74.725),
(239, 32, 31, 2.3, 3.67, 74.85),
(240, 33, 36, 3.55, 3.58, 73.875),
(241, 34, 30, 2.6, 3.6, 67.8),
(242, 35, 35, 3.1, 3.5, 71.5),
(243, 36, 32, 3.2, 3.5, 70.5),
(244, 37, 35, 3.6, 3.6, 74.6),
(245, 38, 31, 2.55, 3.55, 73.775),
(246, 39, 32, 3.55, 3.7, 71.175),
(247, 40, 34, 3.5, 3.53, 72.375),
(248, 41, 34, 3.3, 3.89, 73.075),
(249, 42, 29, 2.3, 3.2, 64.575),
(250, 43, 32, 3, 3.5, 70),
(251, 44, 31, 3.25, 3.5, 72.5),
(252, 45, 36, 3.3, 3, 75),
(253, 46, 35, 2.5, 3.2, 64.575),
(254, 47, 35, 3.55, 3.7, 67.5),
(255, 48, 30, 3.8, 3, 70.175),
(256, 49, 34, 3.3, 3.6, 72),
(257, 50, 38, 3.5, 3.7, 75);

-- --------------------------------------------------------

--
-- Table structure for table `aslab_terpilih`
--

CREATE TABLE `aslab_terpilih` (
  `id` int(11) NOT NULL,
  `calon` int(11) NOT NULL,
  `nilai` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aslab_terpilih`
--

INSERT INTO `aslab_terpilih` (`id`, `calon`, `nilai`) VALUES
(1, 29, 0.537896),
(2, 30, 0.475734),
(3, 31, 0.537896),
(4, 32, 0.502109),
(5, 33, 0.537896),
(6, 34, 0.493338),
(7, 35, 0.537896),
(8, 36, 0.537896),
(9, 37, 0.537896),
(10, 38, 0.520664),
(11, 39, 0.537896),
(12, 40, 0.537896),
(13, 41, 0.537896),
(14, 42, 0.453773),
(15, 43, 0.520664),
(16, 44, 0.537896),
(17, 45, 0.524266),
(18, 46, 0.493338),
(19, 47, 0.511367),
(20, 48, 0.524266),
(21, 49, 0.537896),
(22, 50, 0.537896);

-- --------------------------------------------------------

--
-- Table structure for table `bobot`
--

CREATE TABLE `bobot` (
  `id` int(11) NOT NULL,
  `wawancara` double DEFAULT NULL,
  `tulis` double DEFAULT NULL,
  `akademik` double DEFAULT NULL,
  `micro` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bobot`
--

INSERT INTO `bobot` (`id`, `wawancara`, `tulis`, `akademik`, `micro`) VALUES
(1, 0.75, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `calon`
--

CREATE TABLE `calon` (
  `id` int(11) NOT NULL,
  `nim` text NOT NULL,
  `nama` text NOT NULL,
  `thn_akademik` text NOT NULL,
  `semester` enum('Ganjil','Genap') NOT NULL,
  `tgl_tes` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calon`
--

INSERT INTO `calon` (`id`, `nim`, `nama`, `thn_akademik`, `semester`, `tgl_tes`) VALUES
(29, '2', 'Wulan Noviana N', '2015-1016', 'Ganjil', '2019-08-19'),
(30, '2', 'Elis', '2015-1016', 'Ganjil', '2019-08-19'),
(31, '2', 'Elsa Maitsa', '2015-1016', 'Genap', '2019-08-19'),
(32, '2', 'Mirna Sulistiani', '2015-1016', 'Genap', '2019-08-19'),
(33, '2', 'Idam Idzin Dimiati', '2015-1016', 'Ganjil', '2019-08-19'),
(34, '2', 'Yani Suryani', '2014-2015', 'Ganjil', '2019-08-19'),
(35, '2', 'Intan Nur Fitri', '2014-2015', 'Ganjil', '2019-08-21'),
(36, '2', 'Assyfa Savitri Damayanti', '2014-2015', 'Ganjil', '2019-08-21'),
(37, '2', 'Ero Rohim', '2014-2015', 'Ganjil', '2019-08-21'),
(38, '2', 'Amelya Puspita Anggraeni', '2015-1016', 'Ganjil', '2019-08-21'),
(39, '2', 'Cici Cahyani', '2014-2015', 'Ganjil', '2019-08-21'),
(40, '2', 'Oky Oktaviani', '2015-1016', 'Ganjil', '2019-08-21'),
(41, '2', 'Edwin Erdiyana', '2014-2015', 'Ganjil', '2019-08-21'),
(42, '2', 'Drajat', '2015-1016', 'Ganjil', '2019-08-21'),
(43, '2', 'Ega Suhandianto', '2014-2015', 'Ganjil', '2019-08-21'),
(44, '2', 'Inda Sidik', '2015-1016', 'Ganjil', '2019-08-21'),
(45, '2', 'Mohamad Akbar Nasirudin', '2015-1016', 'Ganjil', '2019-08-21'),
(46, '2', 'Santi Susanti', '2014-2015', 'Ganjil', '2019-08-21'),
(47, '2', 'M. Ridwan Firdaus', '2015-1016', 'Ganjil', '2019-08-21'),
(48, '2', 'Mohamad Rizqi Muladi', '2014-2015', 'Ganjil', '2019-08-21'),
(49, '2', 'Novia Dwi Anggraeni', '2014-2015', 'Ganjil', '2019-08-21'),
(50, '2', 'Pradika Gusti Aryable', '2015-1016', 'Ganjil', '2019-08-21');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kriteria`
--

CREATE TABLE `nilai_kriteria` (
  `kriteria` double NOT NULL,
  `wawancara` double DEFAULT NULL,
  `tulis` double DEFAULT NULL,
  `akademik` double DEFAULT NULL,
  `micro` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_kriteria`
--

INSERT INTO `nilai_kriteria` (`kriteria`, `wawancara`, `tulis`, `akademik`, `micro`) VALUES
(0, 1.4, 23, 1.4, 58),
(0.5, 1.5, 24, 1.5, 59),
(0.75, 2.4, 30, 2.4, 70),
(1, 3.1, 41, 3.1, 82);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_tes`
--

CREATE TABLE `nilai_tes` (
  `id` int(11) NOT NULL,
  `calon` int(11) NOT NULL,
  `tulis` double DEFAULT NULL,
  `wawancara` double DEFAULT NULL,
  `akademik` double DEFAULT NULL,
  `micro` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_tes`
--

INSERT INTO `nilai_tes` (`id`, `calon`, `tulis`, `wawancara`, `akademik`, `micro`) VALUES
(236, 29, 0.75, 1, 1, 0.75),
(237, 30, 0.75, 0.5, 1, 0.5),
(238, 31, 0.75, 1, 1, 0.75),
(239, 32, 0.75, 0.5, 1, 0.75),
(240, 33, 0.75, 1, 1, 0.75),
(241, 34, 0.75, 0.75, 1, 0.5),
(242, 35, 0.75, 1, 1, 0.75),
(243, 36, 0.75, 1, 1, 0.75),
(244, 37, 0.75, 1, 1, 0.75),
(245, 38, 0.75, 0.75, 1, 0.75),
(246, 39, 0.75, 1, 1, 0.75),
(247, 40, 0.75, 1, 1, 0.75),
(248, 41, 0.75, 1, 1, 0.75),
(249, 42, 0.5, 0.5, 1, 0.5),
(250, 43, 0.75, 0.75, 1, 0.75),
(251, 44, 0.75, 1, 1, 0.75),
(252, 45, 0.75, 1, 0.75, 0.75),
(253, 46, 0.75, 0.75, 1, 0.5),
(254, 47, 0.75, 1, 1, 0.5),
(255, 48, 0.75, 1, 0.75, 0.75),
(256, 49, 0.75, 1, 1, 0.75),
(257, 50, 0.75, 1, 1, 0.75);

-- --------------------------------------------------------

--
-- Table structure for table `penguji`
--

CREATE TABLE `penguji` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `jabatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penguji`
--

INSERT INTO `penguji` (`id`, `nama`, `jabatan`) VALUES
(2, 'Tito Sugiharto', 'Kaprodi'),
(3, 'Dadan Nugraha', 'Sekretaris Prodi');

-- --------------------------------------------------------

--
-- Table structure for table `p_kriteria`
--

CREATE TABLE `p_kriteria` (
  `id` int(11) NOT NULL,
  `kriteria` text NOT NULL,
  `w` double DEFAULT NULL,
  `t` double DEFAULT NULL,
  `a` double DEFAULT NULL,
  `m` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_kriteria`
--

INSERT INTO `p_kriteria` (`id`, `kriteria`, `w`, `t`, `a`, `m`) VALUES
(1, 'w', 1, 0.2, 0.17, 0.3),
(2, 't', 5, 1, 0.5, 3),
(3, 'a', 6, 2, 1, 5),
(4, 'm', 3, 0.3, 0.2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tes_nilai`
--

CREATE TABLE `tes_nilai` (
  `id` int(11) NOT NULL,
  `jenis_tes` text NOT NULL,
  `calon` int(11) NOT NULL,
  `nilai` double NOT NULL,
  `penguji` text NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tes_nilai`
--

INSERT INTO `tes_nilai` (`id`, `jenis_tes`, `calon`, `nilai`, `penguji`, `ket`) VALUES
(41, 'Test Tulis', 29, 30, 'Tito Sugiharto', 'tulis'),
(42, 'Test Wawancara', 29, 3.15, 'Tito Sugiharto', 'wawancara'),
(43, 'Nilai Akademik', 29, 3.5, '-', 'akademik'),
(44, 'Test Microteacing', 29, 70.1, 'Tito Sugiharto', 'micro'),
(45, 'Test Wawancara', 30, 2, 'Tito Sugiharto', 'wawancara'),
(46, 'Test Tulis', 30, 36, 'Tito Sugiharto', 'tulis'),
(47, 'Nilai Akademik', 30, 3.63, '-', 'akademik'),
(48, 'Test Microteacing', 30, 64.15, 'Tito Sugiharto', 'micro'),
(49, 'Test Wawancara', 31, 3.5, 'Dadan Nugraha', 'wawancara'),
(50, 'Test Tulis', 31, 38, 'Tito Sugiharto', 'tulis'),
(51, 'Nilai Akademik', 31, 3.5, '-', 'akademik'),
(52, 'Test Microteacing', 31, 74.725, 'Tito Sugiharto', 'micro'),
(53, 'Test Wawancara', 32, 2.3, 'Tito Sugiharto', 'wawancara'),
(54, 'Test Tulis', 32, 31, 'Tito Sugiharto', 'tulis'),
(55, 'Nilai Akademik', 32, 3.67, '-', 'akademik'),
(56, 'Test Microteacing', 32, 74.85, 'Dadan Nugraha', 'micro'),
(57, 'Test Wawancara', 33, 3.55, 'Tito Sugiharto', 'wawancara'),
(58, 'Test Tulis', 33, 36, 'Tito Sugiharto', 'tulis'),
(59, 'Nilai Akademik', 33, 3.58, '-', 'akademik'),
(60, 'Test Microteacing', 33, 73.875, 'Tito Sugiharto', 'micro'),
(61, 'Test Wawancara', 34, 2.6, 'Tito Sugiharto', 'wawancara'),
(62, 'Test Tulis', 34, 30, 'Tito Sugiharto', 'tulis'),
(63, 'Nilai Akademik', 34, 3.6, '-', 'akademik'),
(64, 'Test Microteacing', 34, 67.8, 'Tito Sugiharto', 'micro'),
(65, 'Test Wawancara', 35, 3.1, 'Tito Sugiharto', 'wawancara'),
(66, 'Test Tulis', 35, 35, 'Tito Sugiharto', 'tulis'),
(67, 'Nilai Akademik', 35, 3.5, '-', 'akademik'),
(68, 'Test Microteacing', 35, 71.5, 'Tito Sugiharto', 'micro'),
(69, 'Test Wawancara', 36, 3.2, 'Tito Sugiharto', 'wawancara'),
(70, 'Test Tulis', 36, 32, 'Tito Sugiharto', 'tulis'),
(71, 'Nilai Akademik', 36, 3.5, '-', 'akademik'),
(72, 'Test Microteacing', 36, 70.5, 'Tito Sugiharto', 'micro'),
(73, 'Test Wawancara', 37, 3.6, 'Tito Sugiharto', 'wawancara'),
(74, 'Test Tulis', 37, 35, 'Tito Sugiharto', 'tulis'),
(75, 'Nilai Akademik', 37, 3.6, '-', 'akademik'),
(76, 'Test Microteacing', 37, 74.6, 'Tito Sugiharto', 'micro'),
(77, 'Test Wawancara', 38, 2.55, 'Tito Sugiharto', 'wawancara'),
(79, 'Nilai Akademik', 38, 3.55, '-', 'akademik'),
(80, 'Test Microteacing', 38, 73.775, 'Tito Sugiharto', 'micro'),
(81, 'Test Wawancara', 40, 3.5, 'Tito Sugiharto', 'wawancara'),
(82, 'Test Tulis', 40, 34, 'Tito Sugiharto', 'tulis'),
(83, 'Nilai Akademik', 40, 3.53, '-', 'akademik'),
(84, 'Test Microteacing', 40, 72.375, 'Tito Sugiharto', 'micro'),
(85, 'Test Wawancara', 41, 3.3, 'Tito Sugiharto', 'wawancara'),
(86, 'Test Tulis', 41, 34, 'Tito Sugiharto', 'tulis'),
(87, 'Nilai Akademik', 41, 3.89, '-', 'akademik'),
(88, 'Test Microteacing', 41, 73.075, 'Tito Sugiharto', 'micro'),
(89, 'Test Wawancara', 42, 2.3, 'Tito Sugiharto', 'wawancara'),
(91, 'Nilai Akademik', 42, 3.2, '-', 'akademik'),
(92, 'Test Microteacing', 42, 64.575, 'Tito Sugiharto', 'micro'),
(93, 'Test Tulis', 42, 29, 'Tito Sugiharto', 'tulis'),
(94, 'Test Wawancara', 43, 3, 'Tito Sugiharto', 'wawancara'),
(95, 'Test Tulis', 43, 32, 'Tito Sugiharto', 'tulis'),
(96, 'Nilai Akademik', 43, 3.5, '-', 'akademik'),
(97, 'Test Microteacing', 43, 70, 'Tito Sugiharto', 'micro'),
(98, 'Test Wawancara', 39, 3.55, 'Tito Sugiharto', 'wawancara'),
(99, 'Test Tulis', 39, 32, 'Tito Sugiharto', 'tulis'),
(100, 'Nilai Akademik', 39, 3.7, '-', 'akademik'),
(101, 'Test Microteacing', 39, 71.175, 'Tito Sugiharto', 'micro'),
(102, 'Test Wawancara', 44, 3.25, 'Tito Sugiharto', 'wawancara'),
(103, 'Test Tulis', 44, 31, 'Tito Sugiharto', 'tulis'),
(104, 'Nilai Akademik', 44, 3.5, '-', 'akademik'),
(105, 'Test Microteacing', 44, 72.5, 'Tito Sugiharto', 'micro'),
(106, 'Test Wawancara', 45, 3.3, 'Tito Sugiharto', 'wawancara'),
(107, 'Test Tulis', 45, 36, 'Tito Sugiharto', 'tulis'),
(108, 'Nilai Akademik', 45, 3, '-', 'akademik'),
(109, 'Test Microteacing', 45, 75, 'Tito Sugiharto', 'micro'),
(110, 'Test Wawancara', 46, 2.5, 'Tito Sugiharto', 'wawancara'),
(111, 'Test Tulis', 46, 35, 'Tito Sugiharto', 'tulis'),
(112, 'Nilai Akademik', 46, 3.2, '-', 'akademik'),
(113, 'Test Microteacing', 46, 64.575, 'Tito Sugiharto', 'micro'),
(114, 'Test Wawancara', 47, 3.55, 'Tito Sugiharto', 'wawancara'),
(115, 'Test Tulis', 47, 35, 'Tito Sugiharto', 'tulis'),
(116, 'Nilai Akademik', 47, 3.7, '-', 'akademik'),
(117, 'Test Microteacing', 47, 67.5, 'Tito Sugiharto', 'micro'),
(118, 'Test Wawancara', 48, 3.8, 'Tito Sugiharto', 'wawancara'),
(119, 'Test Tulis', 48, 30, 'Tito Sugiharto', 'tulis'),
(120, 'Nilai Akademik', 48, 3, '-', 'akademik'),
(121, 'Test Microteacing', 48, 70.175, 'Tito Sugiharto', 'micro'),
(122, 'Test Wawancara', 49, 3.3, 'Tito Sugiharto', 'wawancara'),
(123, 'Test Tulis', 49, 34, 'Tito Sugiharto', 'tulis'),
(124, 'Nilai Akademik', 49, 3.6, '-', 'akademik'),
(125, 'Test Microteacing', 49, 72, 'Tito Sugiharto', 'micro'),
(126, 'Test Wawancara', 50, 3.5, 'Tito Sugiharto', 'wawancara'),
(127, 'Test Tulis', 50, 38, 'Tito Sugiharto', 'tulis'),
(128, 'Nilai Akademik', 50, 3.7, '-', 'akademik'),
(129, 'Test Microteacing', 50, 75, 'Tito Sugiharto', 'micro'),
(131, 'Test Tulis', 38, 31, 'Tito Sugiharto', 'tulis');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`) VALUES
(1, 'Siti Maesyaroh', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ahp_aslab_terpilih`
--
ALTER TABLE `ahp_aslab_terpilih`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ahp_nilai_tes`
--
ALTER TABLE `ahp_nilai_tes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aslab_terpilih`
--
ALTER TABLE `aslab_terpilih`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bobot`
--
ALTER TABLE `bobot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calon`
--
ALTER TABLE `calon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_kriteria`
--
ALTER TABLE `nilai_kriteria`
  ADD PRIMARY KEY (`kriteria`);

--
-- Indexes for table `nilai_tes`
--
ALTER TABLE `nilai_tes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penguji`
--
ALTER TABLE `penguji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_kriteria`
--
ALTER TABLE `p_kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tes_nilai`
--
ALTER TABLE `tes_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ahp_aslab_terpilih`
--
ALTER TABLE `ahp_aslab_terpilih`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ahp_nilai_tes`
--
ALTER TABLE `ahp_nilai_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `aslab_terpilih`
--
ALTER TABLE `aslab_terpilih`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `bobot`
--
ALTER TABLE `bobot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `calon`
--
ALTER TABLE `calon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `nilai_tes`
--
ALTER TABLE `nilai_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `penguji`
--
ALTER TABLE `penguji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `p_kriteria`
--
ALTER TABLE `p_kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tes_nilai`
--
ALTER TABLE `tes_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
