<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Penguji</li>
    </ol>
</section>
<section class="content">
    <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Data Penguji</b></h4>
        </div>
        <div class="box-body">
            <a class="btn btn-success" data-toggle="modal" href='#modal-id'>Tambah Data</a>

            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $query_tampil =mysqli_query($con,"select * from penguji");
                        while($p = mysqli_fetch_object($query_tampil)){?>
                        <tr>
                            <td><?=$p->id?></td>
                            <td><?=$p->nama?></td>
                            <td><?=$p->jabatan?></td>
                            <td>
                                <button class="btn btn-success" onclick="edit('<?=$p->id?>')">Edit</button>
                                <a href="pages/penguji/hapus.php?id=<?=$p->id?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</section>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Penguji</h4>
            </div>
            <div class="modal-body">

                <form action="pages/penguji/save_penguji.php" method="POST" class="" role="form">
                <div class="form-group">
                        <label for="">Id</label>
                        <?php
                            $query =mysqli_query($con,"select COUNT(id)+1 as id from penguji");
                            $id = mysqli_fetch_array($query);
                        ?>
                        <input type="text" readonly name="id" value="<?=$id['id']?>"class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Penguji</label>
                        <input type="text" name="nama" placeholder="Nama Penguji" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Jabatan</label>
                        <select name="jabatan" id="" class="form-control">
                            <option value="Dekan">Dekan</option>
                            <option value="Kaprodi">Kaprodi</option>
                            <option value="Sekretaris Prodi">Sekretaris Prodi</option>
                            <option value="Kepala Lab">Kepala Lab</option>
                            <option value="Sekretaris Lab">Sekretaris Lab</option>
                        </select>
                    </div>
               

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" >Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Penguji</h4>
            </div>
            <div class="modal-body" id="m_edit">

                
            </div>
        </div>
    </div>
</div>
<script>
function edit(id){
    var data ='id='+id;
    $.ajax({
        url :"pages/penguji/form-edit.php",
        data :data,
        type:"post",
        success:function(e){
            $("#m_edit").html(e);
            $('#modal-edit').modal("show");
        }
    });
}
</script>