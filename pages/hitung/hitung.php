<?php
include "../koneksi/koneksi.php";
$qx =mysqli_query($con,"SELECT SQRT(SUM(POW(tulis,2))) as tulis,SQRT(SUM(POW(wawancara,2))) as wawancara,SQRT(SUM(POW(akademik,2))) as akademik,SQRT(SUM(POW(micro,2))) as micro FROM `nilai_tes`");
$x =mysqli_fetch_object($qx);
$normal =array();
$qn =mysqli_query($con,"select * from nilai_tes");
while($d = mysqli_fetch_object($qn)){
    $da=array(
        "calon"=>$d->calon,
        "wawancara"=>$d->wawancara/$x->wawancara,
        "tulis"=>$d->tulis/$x->tulis,
        "akademik"=>$d->akademik/$x->akademik,
        "micro"=>$d->micro/$x->micro
    );
    array_push($normal,$da);
}
$qb =mysqli_query($con,"select * from bobot");
$bobot =mysqli_fetch_object($qb);
$tbl_y=array();
foreach($normal as $n){
    $db=array(
        "calon"=>$n['calon'],
        "wawancara"=>$n['wawancara']*$bobot->wawancara,
        "tulis"=>$n['tulis']*$bobot->tulis,
        "akademik"=>$n['akademik']*$bobot->akademik,
        "micro"=>$n['micro']*$bobot->micro
    );
    array_push($tbl_y,$db);
}
$array_wawancara =array();
$array_tulis =array();
$array_akademik =array();
$array_micro =array();

foreach($tbl_y as $y){
    array_push($array_wawancara,$y['wawancara']);
    array_push($array_tulis,$y['tulis']);
    array_push($array_akademik,$y['akademik']);
    array_push($array_micro,$y['micro']);
}
$tbl_solus_plus =array(
    'wawancara'=>getMax($array_wawancara),
    'tulis'=>getMax($array_tulis),
    'akademik'=>getMax($array_akademik),
    'micro'=>getMax($array_micro),
);
$tbl_solus_min =array(
    'wawancara'=>getMin($array_wawancara),
    'tulis'=>getMin($array_tulis),
    'akademik'=>getMin($array_akademik),
    'micro'=>getMin($array_micro),
);
$tbl_d =array();
foreach($tbl_y as $y){
    $d_plush =sqrt(pow($tbl_solus_plus['wawancara']-$y['wawancara'],2)+pow($tbl_solus_plus['tuis']-$y['tulis'],2)+pow($tbl_solus_plus['akademik']-$y['akademik'],2)+pow($tbl_solus_plus['micro']-$y['micro'],2));
    $d_min =sqrt(pow($tbl_solus_min['wawancara']-$y['wawancara'],2)+pow($tbl_solus_min['tuis']-$y['tulis'],2)+pow($tbl_solus_min['akademik']-$y['akademik'],2)+pow($tbl_solus_min['micro']-$y['micro'],2));
    $d_sol=array(
        'calon'=>$y['calon'],
        'd_plus'=>$d_plush,
        'd_min'=>$d_min
    );
    array_push($tbl_d,$d_sol);
}
$tbl_prevensi=array();
foreach($tbl_d as $ds){
    $np =$ds['d_min']/($ds['d_min']+$ds['d_plus']);
    $dp =array(
        'calon'=>$ds['calon'],
        'nilai'=>number_format($np,6)
    );
    array_push($tbl_prevensi,$dp);
}
foreach($tbl_prevensi as $p){
    $calon =$p['calon'];
    $nilai =$p['nilai'];
    mysqli_query($con,"insert into aslab_terpilih values(NULL,'$calon','$nilai')");
}


function getMax($array)  
{ 
   $n = count($array);  
   $max = $array[0]; 
   for ($i = 1; $i < $n; $i++)  
       if ($max < $array[$i]) 
           $max = $array[$i]; 
    return $max;        
} 
  
// Returns maximum in array 
function getMin($array)  
{ 
   $n = count($array);  
   $min = $array[0]; 
   for ($i = 1; $i < $n; $i++)  
       if ($min > $array[$i]) 
           $min = $array[$i]; 
    return $min;        
} 
include "ahp_hitung.php";
header("location:../../index.php?pages=penerimaan");
?>