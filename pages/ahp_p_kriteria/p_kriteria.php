<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calon Aslab</li>
    </ol>
</section>
<section class="content">
    <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Data Nilai Perbandingan Kriteria</b></h4>
        </div>
        <div class="box-body">
            
            <form action="pages/ahp_p_kriteria/save.php" method="POST" class="form-horizontal" role="form">
                
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="bg-orange">Perbandingan Kriteria</th>
                                <th class="bg-primary">Wawancara</th>
                                <th class="bg-primary">Tes Tertulis</th>
                                <th class="bg-primary">Nilai Akademik</th>
                                <th class="bg-primary">Microteaching</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $query="SELECT * FROM p_kriteria ORDER BY id  ASC";
                            $query_tampil =mysqli_query($con,$query);
                            while($d =mysqli_fetch_object($query_tampil)){
                                if($d->kriteria =='w'){
                            ?>
                            <tr>
                                <td class="bg-primary">Wawancara</td>
                                <td><input type="text" value="<?=$d->w?>" readonly name="w_w" class="form-control"></td>
                                <td><input type="text" value="<?=$d->t?>" name="w_t" class="form-control"></td>
                                <td><input type="text" value="<?=$d->a?>" name="w_a" class="form-control"></td>
                                <td><input type="text" value="<?=$d->m?>" name="w_m" class="form-control"></td>
                            </tr>
                                <?php }else if($d->kriteria =='t'){?>
                            <tr>
                                <td class="bg-primary">Tes Tulis</td>
                                <td><input type="text" name="t_w" class="form-control" value="<?=$d->w?>"></td>
                                <td><input type="text" readonly name="t_t" class="form-control" value="<?=$d->t?>"></td>
                                <td><input type="text" name="t_a" class="form-control" value="<?=$d->a?>"></td>
                                <td><input type="text" name="t_m" class="form-control" value="<?=$d->m?>"></td>
                            </tr>
                            <?php }else if($d->kriteria =='a'){?>
                            <tr>
                                <td class="bg-primary">Nilai Akademik</td>
                                <td><input type="text" name="a_w" class="form-control" value="<?=$d->w?>"></td>
                                <td><input type="text" name="a_t" class="form-control" value="<?=$d->t?>"></td>
                                <td><input type="text" readonly name="a_a" class="form-control" value="<?=$d->a?>"></td>
                                <td><input type="text" name="a_m" class="form-control" value="<?=$d->m?>"></td>
                            </tr>
                            <?php }else if($d->kriteria =='m'){?>
                            <tr>
                                <td class="bg-primary">Microteaching</td>
                                <td><input type="text" name="m_w" class="form-control" value="<?=$d->w?>"></td>
                                <td><input type="text" name="m_t" class="form-control" value="<?=$d->t?>"></td>
                                <td><input type="text" name="m_a" class="form-control" value="<?=$d->a?>"></td>
                                <td><input type="text" readonly name="m_m" class="form-control" value="<?=$d->m?>"></td>
                            </tr>
                            <?php } }?>
                        </tbody>
                    </table>
                </div>
                
                <div class="form-group">
                    <label for="" class="col-sm-3"></label>
                    <div class="col-sm-4">
                    <button class="btn btn-primary">simpan</button>
                    </div>
                </div>
            </form>
            

            

        </div>
    </div>

</section>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Nilai Aslab</h4>
            </div>
            <div class="modal-body">

                <form action="pages/aslab/save_nilai_tes.php" method="POST" class="" role="form">
                    <div class="form-group">
                        <label for="">Id Test</label>
                        <?php
                            $query =mysqli_query($con,"select COUNT(id)+1 as id from tes_nilai");
                            $id = mysqli_fetch_array($query);
                        ?>
                        <input type="text" readonly name="id" value="<?=$id['id']?>" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Tes</label>
                        <select name="jenis_tes" id="" class="form-control">
                            <option value="tulis">Test Tulis</option>
                            <option value="wawancara">Test Wawancara</option>
                            <option value="micro">Test Microteacing</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Calon Aslab</label>
                        <select name="calon" id="" class="form-control">
                            <?php
                            $query_calon =mysqli_query($con,"select id,nama from calon");
                            while($c = mysqli_fetch_object($query_calon)){?>
                            <option value="<?=$c->id?>"><?=$c->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Penguji</label>
                        <select name="penguji" id="" class="form-control">
                            <?php
                                $query_penguji =mysqli_query($con,"select nama from penguji");
                                while($p = mysqli_fetch_object($query_penguji)){?>
                            <option value="<?=$p->nama?>"><?=$p->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Nilai</label>
                        <input type="text" name="nilai" class="form-control">
                        
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>