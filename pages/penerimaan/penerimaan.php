<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calon Aslab</li>
    </ol>
</section>
<section class="content">
    <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Data Penerimaan Aslab</b></h4>
        </div>
        <div class="box-body">
            <?php 
            $row = mysqli_fetch_row(mysqli_query($con,"SELECT a.*,b.nama FROM `aslab_terpilih` as a join calon as b on a.calon=b.id ORDER by a.nilai DESC"));
            if($row ==0){?>
            <a class="btn btn-success" href="pages/hitung/hitung.php" >Hitung</a>
            <?php } else{?>
                <a class="btn btn-danger " href="pages/penerimaan/hapus.php" >Hapus Data</a>
            <?php }?>

            <center><h4>Dengan Metode TOPSIS</h4></center>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th>No</th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Rangking</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no=1;
                        $query_tampil =mysqli_query($con,"SELECT a.*,b.nama,b.nim FROM `aslab_terpilih` as a join calon as b on a.calon=b.id ORDER by a.nilai DESC");
                        while($p = mysqli_fetch_object($query_tampil)){?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$p->nim?></td>
                            <td><?=$p->nama?></td>
                            <td><?=$no?></td>
                            <?php
                            $ket ="";
                            if($p->nilai >=0 && $p->nilai <=0.490 ){
                                $ket ="Ditolak";
                            }else if($p->nilai >0.490 && $p->nilai <=0.750 ){
                                $ket ="Dipertimbangkan";
                            }else if($p->nilai >0.750 && $p->nilai <=1 ){
                                $ket ="Diterima";
                            }
                            ?>
                            <td><?=$ket?></td>
                        </tr>
                        <?php $no++;} ?>
                    </tbody>
                </table>
            </div>
            <center><h4>Dengan Metode AHP</h4></center>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th>No</th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Rangking</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no=1;
                        $query_tampil =mysqli_query($con,"SELECT a.*,b.nama,b.nim FROM `ahp_aslab_terpilih` as a join calon as b on a.calon=b.id ORDER by a.nilai DESC");
                        while($ahp = mysqli_fetch_object($query_tampil)){?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$ahp->nim?></td>
                            <td><?=$ahp->nama?></td>
                            <td><?=$no?></td>
                            <?php
                            $ket ="";
                            if($ahp->nilai >=0 && $ahp->nilai <=0.0490 ){
                                $ket ="Ditolak";
                            }else if($ahp->nilai >0.0490 && $ahp->nilai <=0.0750 ){
                                $ket ="Dipertimbangkan";
                            }else if($ahp->nilai >0.0750 && $ahp->nilai <=0.10 ){
                                $ket ="Diterima";
                            }else if($ahp->nilai >0.10 ){
                                $ket ="Diterima";
                            }
                            ?>
                            <td><?=$ket?></td>
                        </tr>
                        <?php $no++;} ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</section>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Calon Aslab</h4>
            </div>
            <div class="modal-body">

                <form action="pages/aslab/save.php" method="POST" class="" role="form">
                <div class="form-group">
                        <label for="">Id</label>
                        <?php
                            $query =mysqli_query($con,"select COUNT(id)+1 as id from calon");
                            $id = mysqli_fetch_array($query);
                        ?>
                        <input type="text" readonly name="id" value="<?=$id['id']?>"class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="">NIM</label>
                        <input type="text" name="nim" placeholder="NIM" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Mahasiswa</label>
                        <input type="text" name="nama" placeholder="Nama Mahasiswa" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Tahun Akademik</label>
                        <input type="text" name="thn_akademik" placeholder="Tahun Akademik" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Semester</label>
                        <select name="semester" id="" class="form-control">
                            <option value="Ganjil">Ganjil</option>
                            <option value="Genap">Genap</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Tes</label>
                        <input type="date" name="tgl_tes" placeholder="Tahun Akademik" class="form-control" required>
                    </div>
               

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" >Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>