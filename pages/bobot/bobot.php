<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calon Aslab</li>
    </ol>
</section>
<section class="content">
    <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Data Bobot Test</b></h4>
        </div>
        <div class="box-body">
            
            <form action="pages/bobot/save.php" method="POST" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="" class="col-sm-3">Tes Tulis</label>
                    <div class="col-sm-4">
                    <select name="tulis" id="" class="form-control">
                        <option value="1">Sangat Penting</option>
                        <option value="0.75">Penting</option>
                        <option value="0.5">Cukup Penting</option>
                        <option value="0">Tidak Penting</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3">Nilai Wawancara</label>
                    <div class="col-sm-4">
                    <select name="wawancara" id="" class="form-control">
                        <option value="1">Sangat Penting</option>
                        <option value="0.75">Penting</option>
                        <option value="0.5">Cukup Penting</option>
                        <option value="0">Tidak Penting</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3">Nilai Akademik</label>
                    <div class="col-sm-4">
                    <select name="akademik" id="" class="form-control">
                        <option value="1">Sangat Penting</option>
                        <option value="0.75">Penting</option>
                        <option value="0.5">Cukup Penting</option>
                        <option value="0">Tidak Penting</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3">Microteaching</label>
                    <div class="col-sm-4">
                    <select name="micro" id="" class="form-control">
                        <option value="1">Sangat Penting</option>
                        <option value="0.75">Penting</option>
                        <option value="0.5">Cukup Penting</option>
                        <option value="0">Tidak Penting</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3"></label>
                    <div class="col-sm-4">
                    <button class="btn btn-primary">simpan</button>
                    </div>
                </div>
            </form>
            

            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            
                            <th>Test Tulis</th>
                            <th>Wawancara</th>
                            <th>Akademik</th>
                            <th>Microteaching</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no=1;
                        $query_tampil =mysqli_query($con,"select * from bobot");
                        while($p = mysqli_fetch_object($query_tampil)){?>
                        <tr>
                            
                            <td><?=$p->tulis?></td>
                            <td><?=$p->wawancara?></td>
                            <td><?=$p->akademik?></td>
                            <td><?=$p->micro?></td>
                        </tr>
                        <?php $no++;} ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</section>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Nilai Aslab</h4>
            </div>
            <div class="modal-body">

                <form action="pages/aslab/save_nilai_tes.php" method="POST" class="" role="form">
                    <div class="form-group">
                        <label for="">Id Test</label>
                        <?php
                            $query =mysqli_query($con,"select COUNT(id)+1 as id from tes_nilai");
                            $id = mysqli_fetch_array($query);
                        ?>
                        <input type="text" readonly name="id" value="<?=$id['id']?>" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Tes</label>
                        <select name="jenis_tes" id="" class="form-control">
                            <option value="tulis">Test Tulis</option>
                            <option value="wawancara">Test Wawancara</option>
                            <option value="micro">Test Microteacing</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Calon Aslab</label>
                        <select name="calon" id="" class="form-control">
                            <?php
                            $query_calon =mysqli_query($con,"select id,nama from calon");
                            while($c = mysqli_fetch_object($query_calon)){?>
                            <option value="<?=$c->id?>"><?=$c->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Penguji</label>
                        <select name="penguji" id="" class="form-control">
                            <?php
                                $query_penguji =mysqli_query($con,"select nama from penguji");
                                while($p = mysqli_fetch_object($query_penguji)){?>
                            <option value="<?=$p->nama?>"><?=$p->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Nilai</label>
                        <input type="text" name="nilai" class="form-control">
                        
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>