<?php
include "../koneksi/koneksi.php";
$calon = $_POST['calon'];
$jenis_tes = $_POST['jenis_tes'];
$penguji = $_POST['penguji'];
$nilai = $_POST['nilai'];
$id = $_POST['id'];
if($jenis_tes =="tulis"){
    mysqli_query($con,"update ahp_nilai_tes set tulis=$nilai where calon='$calon'");
    mysqli_query($con,"update tes_nilai set jenis_tes='Test Tulis',calon='$calon',nilai='$nilai',penguji='$penguji',ket='$jenis_tes' where id='$id'");
    $query_k = mysqli_query($con,"SELECT kriteria,tulis FROM `nilai_kriteria` ORDER BY `kriteria`  DESC");
    
    $sm ="";
    $m ="";
    $c ="";
    $tm ="";

    $w_sm ="";
    $w_m ="";
    $w_c ="";
    $w_tm ="";
    while($w =mysqli_fetch_object($query_k)){
        if($w->kriteria ==1){
            $sm =$w->kriteria;
            $w_sm =$w->tulis;
        }else if($w->kriteria ==0.75){
            $m =$w->kriteria;
            $w_m =$w->tulis;
        }else if($w->kriteria ==0.5){
            $c =$w->kriteria;
            $w_c =$w->tulis;
        }else if($w->kriteria ==0){
            $tm =$w->kriteria;
            $w_tm =$w->tulis;
        }
    }
    $nf="";
    if($nilai >=$w_sm){
        $nf =$sm;
    }else if($nilai < $w_sm && $nilai >=$w_m){
        $nf =$m;
    }else if($nilai >=$w_c && $nilai <$w_m){
        $nf =$c;
    }else if($nilai >=$w_tm && $nilai <$w_c){
        $nf =$tm;
    }else if($nilai <$w_tm){
        $nf =$tm;
    }
    mysqli_query($con,"update nilai_tes set tulis=$nf where calon='$calon'");
    
}else if($jenis_tes =="wawancara"){
    mysqli_query($con,"update ahp_nilai_tes set wawancara=$nilai where calon='$calon'");
    mysqli_query($con,"update tes_nilai set jenis_tes='Test Wawancara',calon='$calon',nilai='$nilai',penguji='$penguji',ket='$jenis_tes' where id='$id'");
    $query_k = mysqli_query($con,"SELECT kriteria,wawancara FROM `nilai_kriteria` ORDER BY `kriteria`  DESC");
    
    $sm ="";
    $m ="";
    $c ="";
    $tm ="";

    $w_sm ="";
    $w_m ="";
    $w_c ="";
    $w_tm ="";
    while($w =mysqli_fetch_object($query_k)){
        if($w->kriteria ==1){
            $sm =$w->kriteria;
            $w_sm =$w->wawancara;
        }else if($w->kriteria ==0.75){
            $m =$w->kriteria;
            $w_m =$w->wawancara;
        }else if($w->kriteria ==0.5){
            $c =$w->kriteria;
            $w_c =$w->wawancara;
        }else if($w->kriteria ==0){
            $tm =$w->kriteria;
            $w_tm =$w->wawancara;
        }
    }
    $nf="";
    if($nilai >=$w_sm){
        $nf =$sm;
    }else if($nilai < $w_sm && $nilai >=$w_m){
        $nf =$m;
    }else if($nilai >=$w_c && $nilai <$w_m){
        $nf =$c;
    }else if($nilai >=$w_tm && $nilai <$w_c){
        $nf =$tm;
    }else if($nilai <$w_tm){
        $nf =$tm;
    }

    mysqli_query($con,"update nilai_tes set wawancara='$nf' where calon='$calon'");
}else if($jenis_tes =="micro"){
    mysqli_query($con,"update ahp_nilai_tes set micro=$nilai where calon='$calon'");
    mysqli_query($con,"update tes_nilai set jenis_tes='Test Microteacing',calon='$calon',nilai='$nilai',penguji='$penguji',ket='$jenis_tes' where id='$id'");
    $query_k = mysqli_query($con,"SELECT kriteria,micro FROM `nilai_kriteria` ORDER BY `kriteria`  DESC");
    
    $sm ="";
    $m ="";
    $c ="";
    $tm ="";

    $w_sm ="";
    $w_m ="";
    $w_c ="";
    $w_tm ="";
    while($w =mysqli_fetch_object($query_k)){
        if($w->kriteria ==1){
            $sm =$w->kriteria;
            $w_sm =$w->micro;
        }else if($w->kriteria ==0.75){
            $m =$w->kriteria;
            $w_m =$w->micro;
        }else if($w->kriteria ==0.5){
            $c =$w->kriteria;
            $w_c =$w->micro;
        }else if($w->kriteria ==0){
            $tm =$w->kriteria;
            $w_tm =$w->micro;
        }
    }
    $nf="";
    if($nilai >=$w_sm){
        $nf =$sm;
    }else if($nilai < $w_sm && $nilai >=$w_m){
        $nf =$m;
    }else if($nilai >=$w_c && $nilai <$w_m){
        $nf =$c;
    }else if($nilai >=$w_tm && $nilai <$w_c){
        $nf =$tm;
    }else if($nilai <$w_tm){
        $nf =$tm;
    }
    mysqli_query($con,"update nilai_tes set micro=$nf where calon='$calon'");
}else if($jenis_tes =="akademik"){
    mysqli_query($con,"update ahp_nilai_tes set akademik=$nilai where calon='$calon'");
    mysqli_query($con,"update tes_nilai set jenis_tes='Nilai Akademik',calon='$calon',nilai='$nilai',penguji='-',ket='$jenis_tes' where id='$id'");
    $query_k = mysqli_query($con,"SELECT kriteria,akademik FROM `nilai_kriteria` ORDER BY `kriteria`  DESC");
    
    $sm ="";
    $m ="";
    $c ="";
    $tm ="";

    $w_sm ="";
    $w_m ="";
    $w_c ="";
    $w_tm ="";
    while($w =mysqli_fetch_object($query_k)){
        if($w->kriteria ==1){
            $sm =$w->kriteria;
            $w_sm =$w->akademik;
        }else if($w->kriteria ==0.75){
            $m =$w->kriteria;
            $w_m =$w->akademik;
        }else if($w->kriteria ==0.5){
            $c =$w->kriteria;
            $w_c =$w->akademik;
        }else if($w->kriteria ==0){
            $tm =$w->kriteria;
            $w_tm =$w->akademik;
        }
    }
    $nf="";
    if($nilai >=$w_sm){
        $nf =$sm;
    }else if($nilai < $w_sm && $nilai >=$w_m){
        $nf =$m;
    }else if($nilai >=$w_c && $nilai <$w_m){
        $nf =$c;
    }else if($nilai >=$w_tm && $nilai <$w_c){
        $nf =$tm;
    }else if($nilai <$w_tm){
        $nf =$tm;
    }
    mysqli_query($con,"update nilai_tes set akademik=$nf where calon='$calon'");
}
header("location:../../index.php?pages=nilai_tes");
?>

