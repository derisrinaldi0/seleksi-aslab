<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calon Aslab</li>
    </ol>
</section>
<section class="content">
    <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Data Nilai Tes</b></h4>
        </div>
        <div class="box-body">
            <a class="btn btn-success" data-toggle="modal" href='#modal-id'>Tambah Data</a>

            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Calon Aslab</th>
                            <th>Jenis Test</th>
                            <th>Penguji</th>
                            <th>Nilai</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no=1;
                        $query_tampil =mysqli_query($con,"SELECT a.*,b.nama FROM `tes_nilai` as a JOIN calon as b on a.calon=b.id order by b.nama asc");
                        while($p = mysqli_fetch_object($query_tampil)){?>
                        <tr>
                            <td><?=$no?></td>
                            <td><?=$p->nama?></td>
                            <td><?=$p->jenis_tes?></td>
                            <td><?=$p->penguji?></td>
                            <td><?=$p->nilai?></td>
                            <td>
                                <button class="btn btn-success" onclick="edit('<?=$p->id?>')">Edit</button>
                                <a href="pages/aslab/hapus_nilai.php?id=<?=$p->id?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                        <?php $no++;} ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</section>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Nilai Aslab</h4>
            </div>
            <div class="modal-body">

                <form action="pages/aslab/save_nilai_tes.php" method="POST" class="" role="form">
                    <div class="form-group">
                        <label for="">Id Test</label>
                        <?php
                            $query =mysqli_query($con,"select COUNT(id)+1 as id from tes_nilai");
                            $id = mysqli_fetch_array($query);
                        ?>
                        <input type="text" readonly name="id" value="<?=$id['id']?>" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Tes</label>
                        <select name="jenis_tes" id="" class="form-control">
                            <option value="tulis">Test Tulis</option>
                            <option value="wawancara">Test Wawancara</option>
                            <option value="micro">Test Microteacing</option>
                            <option value="akademik">Nilai Akademik</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Calon Aslab</label>
                        <select name="calon" id="" class="form-control">
                            <?php
                            $query_calon =mysqli_query($con,"select id,nama from calon");
                            while($c = mysqli_fetch_object($query_calon)){?>
                            <option value="<?=$c->id?>"><?=$c->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Penguji</label>
                        <select name="penguji" id="" class="form-control">
                            <?php
                                $query_penguji =mysqli_query($con,"select nama from penguji");
                                while($p = mysqli_fetch_object($query_penguji)){?>
                            <option value="<?=$p->nama?>"><?=$p->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Nilai</label>
                        <input type="text" name="nilai" class="form-control">
                        
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Calon Aslab</h4>
            </div>
            <div class="modal-body" id="m_edit">

                
            </div>
        </div>
    </div>
</div>
<script>
function edit(id){
    var data ='id='+id;
    $.ajax({
        url :"pages/aslab/form-edit-nilai.php",
        data :data,
        type:"post",
        success:function(e){
            $("#m_edit").html(e);
            $('#modal-edit').modal("show");
        }
    });
}
</script>