<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calon Aslab</li>
    </ol>
</section>
<section class="content">
    <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Data Nilai Kriteria</b></h4>
        </div>
        <div class="box-body">
            
            <form action="pages/nilai_kriteria/save.php" method="POST" class="form-horizontal" role="form">
                
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Kriteria</th>
                                <th>Nilai Wawancara</th>
                                <th>Nilai Tertulis</th>
                                <th>Nilai Akademik</th>
                                <th>Nilai Microteaching</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $query="SELECT * FROM `nilai_kriteria` ORDER BY kriteria  DESC";
                            $query_tampil =mysqli_query($con,$query);
                            while($d =mysqli_fetch_object($query_tampil)){
                                if($d->kriteria ==1){
                            ?>
                            <tr>
                                <td>Sangat Memuaskan</td>
                                <td><input type="text" value="<?=$d->wawancara?>" name="wawancara_sm" class="form-control"></td>
                                <td><input type="text" value="<?=$d->tulis?>" name="tulis_sm" class="form-control"></td>
                                <td><input type="text" value="<?=$d->akademik?>" name="akademik_sm" class="form-control"></td>
                                <td><input type="text" value="<?=$d->micro?>" name="micro_sm" class="form-control"></td>
                            </tr>
                                <?php }else if($d->kriteria ==0.75){?>
                            <tr>
                                <td>Memuaskan</td>
                                <td><input type="text" name="wawancara_m" class="form-control" value="<?=$d->wawancara?>"></td>
                                <td><input type="text" name="tulis_m" class="form-control" value="<?=$d->tulis?>"></td>
                                <td><input type="text" name="akademik_m" class="form-control" value="<?=$d->akademik?>"></td>
                                <td><input type="text" name="micro_m" class="form-control" value="<?=$d->micro?>"></td>
                            </tr>
                            <?php }else if($d->kriteria ==0.5){?>
                            <tr>
                                <td>Cukup</td>
                                <td><input type="text" name="wawancara_c" class="form-control" value="<?=$d->wawancara?>"></td>
                                <td><input type="text" name="tulis_c" class="form-control" value="<?=$d->tulis?>"></td>
                                <td><input type="text" name="akademik_c" class="form-control" value="<?=$d->akademik?>"></td>
                                <td><input type="text" name="micro_c" class="form-control" value="<?=$d->micro?>"></td>
                            </tr>
                            <?php }else if($d->kriteria ==0){?>
                            <tr>
                                <td>Sangat Tidak Memuaskan</td>
                                <td><input type="text" name="wawancara_tm" class="form-control" value="<?=$d->wawancara?>"></td>
                                <td><input type="text" name="tulis_tm" class="form-control" value="<?=$d->tulis?>"></td>
                                <td><input type="text" name="akademik_tm" class="form-control" value="<?=$d->akademik?>"></td>
                                <td><input type="text" name="micro_tm" class="form-control" value="<?=$d->micro?>"></td>
                            </tr>
                            <?php } }?>
                        </tbody>
                    </table>
                </div>
                
                <div class="form-group">
                    <label for="" class="col-sm-3"></label>
                    <div class="col-sm-4">
                    <button class="btn btn-primary">simpan</button>
                    </div>
                </div>
            </form>
            

            

        </div>
    </div>

</section>


<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Nilai Aslab</h4>
            </div>
            <div class="modal-body">

                <form action="pages/aslab/save_nilai_tes.php" method="POST" class="" role="form">
                    <div class="form-group">
                        <label for="">Id Test</label>
                        <?php
                            $query =mysqli_query($con,"select COUNT(id)+1 as id from tes_nilai");
                            $id = mysqli_fetch_array($query);
                        ?>
                        <input type="text" readonly name="id" value="<?=$id['id']?>" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Tes</label>
                        <select name="jenis_tes" id="" class="form-control">
                            <option value="tulis">Test Tulis</option>
                            <option value="wawancara">Test Wawancara</option>
                            <option value="micro">Test Microteacing</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Calon Aslab</label>
                        <select name="calon" id="" class="form-control">
                            <?php
                            $query_calon =mysqli_query($con,"select id,nama from calon");
                            while($c = mysqli_fetch_object($query_calon)){?>
                            <option value="<?=$c->id?>"><?=$c->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Penguji</label>
                        <select name="penguji" id="" class="form-control">
                            <?php
                                $query_penguji =mysqli_query($con,"select nama from penguji");
                                while($p = mysqli_fetch_object($query_penguji)){?>
                            <option value="<?=$p->nama?>"><?=$p->nama?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Nilai</label>
                        <input type="text" name="nilai" class="form-control">
                        
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>