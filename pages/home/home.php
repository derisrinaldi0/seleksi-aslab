<section class="content-header">
    <h1>
        Aplikasi Penerimaan dan Kompetensi Aslab
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calon Aslab</li>
    </ol>
</section>
<section class="content">
    <!-- <div class="callout-info callout">
        <h4>Tips !</h4>
        <p>Silahkan memilih salah satu aksi</p>
    </div> -->
    <div class="box">
        <div class="box-header with-border">
            <h4><b>Home</b></h4>
        </div>
        <div class="box-body">
            
            <div id="carousel-id"  class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-id" data-slide-to="0" class=""></li>
                    <li data-target="#carousel-id" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-id" data-slide-to="2" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item" style="height:500px">
                        <img data-src="images/aslab1.jpeg" alt="First slide" src="images/aslab1.jpeg">
                        <div class="container">
                            <div class="carousel-caption">
                                
                            </div>
                        </div>
                    </div>
                    <div class="item" style="height:500px">
                        <img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide" src="images/aslab2.jpeg">
                        <div class="container">
                            <div class="carousel-caption">
                                
                            </div>
                        </div>
                    </div>
                    <div class="item active"  style="height:500px">
                        <img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide" src="images/aslab3.jpeg">
                        <div class="container">
                            <div class="carousel-caption">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            
        </div>
    </div>

</section>


